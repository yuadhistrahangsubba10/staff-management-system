<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->bigInteger('cid')->primary();
            $table->string('email')->unique();
            $table->string('name');
            $table->integer('phone');
            $table->string('specialization');
            $table->string('gender');
            $table->string('location');
            $table->string('hospitalName');
            $table->string('description',2000);
            $table->string("image")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('doctors');
    }
};
