<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MovementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('movement')->insert([
            [
                'cid' => '11807001111',
                'name' => 'Tenzin',
                'start' => '2024/03/01',
                'end' => '2024/03/03',
                'type' => 'leave',
                'remarks' => 'Going on medical checkup',
                'file' => Null,
                'location' => 'Thimphu',
                'hospitalName' => 'JDWNRH',
            ],
            [
                'cid' => '11807002222',
                'name' => 'Dorji',
                'start' => '2024/03/02',
                'end' => '2024/03/04',
                'type' => 'leave',
                'remarks' => 'Going on medical checkup',
                'file' => Null,
                'location' => 'Thimphu',
                'hospitalName' => 'JDWNRH',
            ],
            [
                'cid' => '11807003333',
                'name' => 'Yangso',
                'start' => '2024/03/06',
                'end' => '2024/03/07',
                'type' => 'tour',
                'remarks' => 'Going on Tour',
                'file' => Null,
                'location' => 'Paro',
                'hospitalName' => 'JDWNRH2',
            ],
            [
                'cid' => '11807004444',
                'name' => 'Dechen',
                'start' => '2024/03/21',
                'end' => '2024/03/23',
                'type' => 'Events',
                'remarks' => 'Going on School',
                'file' => Null,
                'location' => 'Mongar',
                'hospitalName' => 'JDWNRH3',

            ],
            [
                'cid' => '11807005555',
                'name' => 'Subba',
                'start' => '2024/03/11',
                'end' => '2024/03/22',
                'type' => 'Tour',
                'remarks' => 'Going on startup meeting',
                'file' => Null,
                'location' => 'Tsirang',
                'hospitalName' => 'JDWNRH4',
            ],
            // Repeating cid
            [
                'cid' => '11807005555',
                'name' => 'Subba',
                'start' => '2024/03/23',
                'end' => '2024/03/25',
                'type' => 'Leave',
                'remarks' => 'Going on Leave',
                'file' => Null,
                'location' => 'Tsirang',
                'hospitalName' => 'JDWNRH4',
            ],
        ]);
    }
}
