<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Staff Movement System</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <!-- Vendor CSS Files -->
  <link rel="stylesheet" href="{{ asset('assets/vendor/aos/aos.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-icons/bootstrap-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/boxicons/css/boxicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/glightbox/css/glightbox.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/swiper/swiper-bundle.min.css') }}">

  <!-- Template Main CSS File -->
  <link rel="stylesheet" href="{{ asset('assets/css/Adduser.css') }}">
</head>
<body>
  <!-- ======= Mobile nav toggle button ======= -->
  <i class="bi bi-list mobile-nav-toggle d-xl-none"></i>
  <!-- ======= Header ======= -->
  <header id="header">
    <div class="d-flex flex-column">
      <div class="profile">
        <img src="{{ asset('assets/img/logo.png') }}" alt="No Logo" class="img-fluid">
        <h1 class="text-light"><a href="index.html"></a></h1>
      </div>
      <nav id="navbar" class="nav-menu navbar">
        <ul>
            <li><a href="{{ url('/admin/dashboard') }}" class="nav-link scrollto"><i class="fas fa-home"></i> <span>DashBoard</span></a></li>
            <li><a href="{{ url('/admin/viewDoctor') }}" class="nav-link scrollto"><i class="fas fa-user-md"></i> <span>Doctor</span></a></li>
            <li><a href="{{ url('/admin/viewHospital') }}" class="nav-link scrollto"><i class="fas fa-hospital"></i> <span>Hospitals</span></a></li>
            <li><a href="{{ url('/admin/viewDirector') }}" class="nav-link scrollto"><i class="fas fa-user-circle"></i> <span>Director</span></a></li>
            <li><a href="{{ url('/admin/viewAdm') }}" class="nav-link scrollto  active"><i class="fas fa-user"></i> <span>Adm</span></a></li>
            <li><a href="{{ url('/admin/Profile') }}" class="nav-link scrollto"><i class="fas fa-user-circle"></i> <span>Profile</span></a></li>
            <li>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <a href="#"  class="nav-link scrollto" onclick="event.preventDefault();this.closest('form').submit();"><i class="fas fa-sign-out-alt"></i><span>Log Out</span></a>
                </form>
            </li>
        </ul>
      </nav>
    </div>
  </header>



  <main id="main">
 <section id="hero" class="about">
  <div class="container text-left">
    <h4 style="font-weight:800; margin: 50px 0px;">Add Adminstrative Assitant</h4>

    @if(Session::has('success'))
        <div class="alert alert-success" role="alert">
            {{ Session::get('success') }}
        </div>
    @endif

    @if(Session::has('error'))
        <div class="alert alert-danger" role="alert">
            {{ Session::get('error') }}
        </div>
    @endif

<form method="POST" action="{{ route('addAdm') }}"  enctype="multipart/form-data">
    {!! csrf_field() !!}
        <div class="form-row">
            <div class="col-lg-4 mb-lg-4 mr-lg-4 mb-3">
                <label>Name</label>
                <input type="text" name="name" id="name" class="form-control rounded-1">
            </div>

            <div class="col-lg-4 mb-lg-4 ml-lg-4 mb-3">
                <label>Email address</label>
                <input type="email" name="email" id="email" class="form-control rounded-1" >
            </div>
        </div>
        <div class="form-row">
            <div class="col-lg-4 mb-lg-4 mr-lg-4 mb-3">
                <label>Phone Number</label>
                <input type="text" name="phone" id="phone" class="form-control rounded-1" >
            </div>

            <div class="col-lg-4 mb-lg-4 ml-lg-4 mb-3">
                <label>CID Number</label>
                <input type="text" name="cid" id="cid" class="form-control rounded-1" >
            </div>
        </div>

        <div class="form-row">
            <div class="col-lg-4 mb-lg-4 mr-lg-4 mb-3">
            <label >Location</label>
            <input type="text" name="location" id="location" class="form-control rounded-1" >
        </div>

            <div class="col-lg-4 mb-lg-4 ml-lg-4 mb-3">
                <label>Hospital</label>
                <input type="text" name="hospitalName" id="hospitalName" class="form-control rounded-1" >
            </div>
        </div>

        <input type="file" name="image" id="image" class="form-control col-lg-3 mt-4" style="font-size: 14px;"/>

        <div class="d-md-flex justify-content-start align-items-center mt-4" style="margin-bottom: 40px;">
          <button class="btn btn-primary " type="submit" >Save</button>
          <button class="btn-2 " type="button">Cancel</button>
      </div>
      </form>

    </div>
</section>
</main><!-- End #main -->
  <!-- Template Main JS File -->
  <script src="{{ asset('assets/js/main.js') }}"></script>
</body>
</html>
