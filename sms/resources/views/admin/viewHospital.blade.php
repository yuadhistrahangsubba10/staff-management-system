<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Staff Movement System</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link rel="stylesheet" href="{{ asset('assets/vendor/aos/aos.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-icons/bootstrap-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/boxicons/css/boxicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/glightbox/css/glightbox.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/swiper/swiper-bundle.min.css') }}">

  <!-- Template Main CSS File -->
  <link rel="stylesheet" href="{{ asset('assets/css/Hospital.css') }}">

</head>

<body>

  <!-- ======= Mobile nav toggle button ======= -->
  <i class="bi bi-list mobile-nav-toggle d-xl-none"></i>

  <!-- ======= Header ======= -->
  <header id="header">
    <div class="d-flex flex-column">

      <div class="profile">
        <img src="{{ asset('assets/img/logo.png') }}" alt="No Logo" class="img-fluid">
        <h1 class="text-light"><a href="index.html"></a></h1>

      </div>

      <nav id="navbar" class="nav-menu navbar">
        <ul>
            <li><a href="{{ url('/admin/dashboard') }}" class="nav-link scrollto"><i class="fas fa-home"></i> <span>DashBoard</span></a></li>
            <li><a href="{{ url('/admin/viewDoctor') }}" class="nav-link scrollto"><i class="fas fa-user-md"></i> <span>Doctor</span></a></li>
            <li><a href="{{ url('/admin/viewHospital') }}" class="nav-link scrollto  active"><i class="fas fa-hospital"></i> <span>Hospitals</span></a></li>
            <li><a href="{{ url('/admin/viewDirector') }}" class="nav-link scrollto"><i class="fas fa-user-circle"></i> <span>Director</span></a></li>
            <li><a href="{{ url('/admin/viewAdm') }}" class="nav-link scrollto "><i class="fas fa-user"></i> <span>Adm</span></a></li>
            <li><a href="{{ url('/admin/Profile') }}" class="nav-link scrollto"><i class="fas fa-user-circle"></i> <span>Profile</span></a></li>
            <li>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <a href="#"  class="nav-link scrollto" onclick="event.preventDefault();this.closest('form').submit();"><i class="fas fa-sign-out-alt"></i><span>Log Out</span></a>
                </form>
            </li>
      </ul>
      </nav>
    </div>
  </header>



  <main id="main">
 <section id="hero" class="about">
  <div class="container text-left">
    <h4 style="font-weight:800;">Hospital List</h4>
       <div class="d-flex justify-content-between align-items-center " style="margin-bottom: 20px;">
        <h4 style="font-weight:800; margin: 0;">
          <div class="dropdown" style="font-size: 14px; display: flex; align-items: center; color: rgb(86, 84, 84);margin-top: 20px;">
            Select Dzongkhag:<a class=" dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false" style="border: 1px solid gray;padding: 5px 42px;margin-left: 20px;border-radius: 5px;text-decoration: none;color: gray;font-weight: 400;">
            Thimphu
            </a>
            <ul class="dropdown-menu">
              <li><a class="dropdown-item" href="#">Thimphu</a></li>
              <li><a class="dropdown-item" href="#">Paro</a></li>
              <li><a class="dropdown-item" href="#">Punakha</a></li>
            </ul>
          </div>
        </h4>

          <div class="d-none d-md-flex justify-content-start align-items-center">
            <button class="btn btn-primary me-md-2" type="button" ><a href="{{ route('addHospital') }}" style="text-decoration: none; color: rgb(255, 255, 255);">Add Hospital</a></button>
            <button class="btn btn-primary" type="button"><a href="{{ route('downloadHospital') }}" style="text-decoration: none; color: rgb(255, 255, 255);">Download</a></button>
        </div>
      </div>

      <div class="d-md-none pr-md-3" >
        <button  href="{{ route('addDoctor') }}" class="btn btn-primary mt-2" type="button">Add Hospital</button>
        <button class="btn btn-primary mt-2" type="button">Download</button>
      </div>

      @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif

        @if($hospitals->isNotEmpty())
            @foreach($hospitals as $hospital)
            <div class="card col-12">
                <img src="{{ asset('/storage/images/' . $hospital->image) }}" alt="{{ $hospital->name }}" class="card-img-top">
                <div class="card-body" style="display: flex;flex-direction: row; align-items: center;">
                    <p class="card-text">
                        <b>{{ $hospital->name }}</b><br>
                        {{ $hospital->location }}<br>
                    </p>
                </div>
                <div class="dropdown">
                    <i class="fas fa-ellipsis-h " id="ellipsisToggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                    <div class="dropdown-menu" aria-labelledby="ellipsisToggle">
                        <a class="dropdown-item" href="{{ route('editHospital', ['id' => $hospital->id]) }}" title="Edit Hospital">Edit</a>

                        <form method="POST" action="{{ route('deleteHospital', ['id' => $hospital->id]) }}" accept-charset="UTF-8" style="display:inline">
                            @method('DELETE')
                            @csrf
                            <a type="submit" class="dropdown-item" title="Delete Hospital" onclick="return confirm('Confirm delete?')">
                                Delete
                            </a>
                        </form>
                    </div>
                </div>
            </div>

            @endforeach
        @else
            <p>No hospitals found.</p>
        @endif

        @if(session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif




  </div>
</section>
</main>
  <script src="{{ asset('assets/js/main.js') }}"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>


</body>

</html>
