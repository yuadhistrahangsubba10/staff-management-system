<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Staff Movement System</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link rel="stylesheet" href="{{ asset('assets/vendor/aos/aos.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-icons/bootstrap-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/boxicons/css/boxicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/glightbox/css/glightbox.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/swiper/swiper-bundle.min.css') }}">

  <!-- Template Main CSS File -->
  <link rel="stylesheet" href="{{ asset('assets/css/Doctor.css') }}">
</head>

<body>
  <!-- ======= Mobile nav toggle button ======= -->
  <i class="bi bi-list mobile-nav-toggle d-xl-none"></i>
  <!-- ======= Header ======= -->
  <header id="header">
    <div class="d-flex flex-column">

      <div class="profile">
        <img src="{{ asset('assets/img/logo.png') }}" alt="No Logo" class="img-fluid">
        <h1 class="text-light"><a href="index.html"></a></h1>

      </div>
      <nav id="navbar" class="nav-menu navbar">
        <ul>
            <li><a href="{{ url('/adm/dashboard') }}" class="nav-link scrollto"><i class="fas fa-home"></i> <span>DashBoard</span></a></li>
            <li><a href="{{ url('/adm/admViewDoctor') }}" class="nav-link scrollto  active"><i class="fas fa-user-md"></i> <span>Doctor</span></a></li>
            <li><a  href="{{ url('/adm/admAddDoctor') }}" class="nav-link scrollto"><i class="fas fa-hospital"></i> <span>Add Doctor</span></a></li>
            <li><a href="{{ url('/adm/leave') }}" class="nav-link scrollto"><i class="fas fa-user-circle"></i> <span>Leave</span></a></li>
            <li><a href="{{ url('/adm/admProfile') }}" class="nav-link scrollto"><i class="fas fa-user-circle"></i> <span>Profile</span></a></li>
            <li>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <a href="#"  class="nav-link scrollto" onclick="event.preventDefault();this.closest('form').submit();"><i class="fas fa-sign-out-alt"></i><span>Log Out</span></a>
                </form>
            </li>
      </ul>
      </nav>
    </div>
  </header>



<main id="main">
 <section id="hero" class="about">
  <div class="container text-left">

      <div class="d-flex justify-content-between align-items-center " style="margin-bottom: 40px;">
        <h4 style="font-weight:800; margin: 0;">Doctor List</h4>
          <div class="d-none d-md-flex justify-content-start align-items-center">
              <button class="btn btn-primary" type="button" style="margin-right: 30px;"><a href="{{ route('downloadLeave') }}" style="margin-right: 30px; text-decoration: none; color: rgb(255, 255, 255);">Download</a></button>
              <button class="btn sort-button" style="color: aliceblue;">
                <i class="fas fa-sort sort-icon"></i> Sort
              </button>
          </div>
      </div>

      <div class="d-md-none mt-3 pr-md-3" style="margin-bottom: 40px;" >
          <button class="btn btn-primary " type="button"><a href="{{ route('downloadLeave') }}" style="margin-right: 30px; text-decoration: none; color: rgb(255, 255, 255);">Download</a></button>
          <button class="btn sort-button" style="color: aliceblue;margin-right: 40px;">
            <i class="fas fa-sort sort-icon"></i> Sort
          </button>
      </div>

      @if(session('success'))
      <div class="alert alert-success">
          {{ session('success') }}
      </div>
  @endif

    {{-- @if($leaves->isEmpty())
    <p>No leave entries found.</p>
    @else
        @foreach($leaves as $leave)
        <div class="card col-12" style="padding: 10px;">
            <div style="cursor: pointer; width: 100%; display: flex; flex-direction: row;">
                    <img src="{{ asset('/storage/images/' . $leave->doctor_image) }}" alt="{{ $leave->name }}" class="card-img-top" style="width: 100px; height: auto; margin-right: 10px;">
                <div class="card-body" style="display: flex; flex-direction: column; justify-content: center;">
                    <p class="card-text mb-0">
                        <b>{{ $leave->name }}</b>
                    </p>
                    <p class="card-text mb-0">CID: {{ $leave->cid }}</p>
                    <p class="card-text mb-0">TYPE: {{ $leave->type }}</p>
                    <p class="card-text mb-0">START: {{ $leave->start }}</p>
                    <p class="card-text mb-0">END: {{ $leave->end }}</p>
                    <p class="card-text mb-0">Leave Status:
                        @if(isset($doctorStatus[$leave->cid]))
                            {{ $doctorStatus[$leave->cid] }}
                        @else
                            Status not available
                        @endif
                    </p>
                </div>
            </div>
        </div>
        @endforeach
    @endif --}}

    @if($doctors->isEmpty())
    <p>No leave entries found.</p>
    @else
        @foreach($doctors as $doctor)
        <div class="card col-12" style="padding: 10px;">
            <div style="cursor: pointer; width: 100%; display: flex; flex-direction: row;">
                    <img src="{{ asset('/storage/images/' . $doctor->image) }}" alt="{{ $doctor->name }}" class="card-img-top" style="width: 100px; height: 100px; margin-right: 10px;">
                <div class="card-body" style="display: flex; flex-direction: column; justify-content: center;">
                    <p class="card-text mb-0">
                        <b>{{ $doctor->name }}</b>
                    </p>
                    <p class="card-text mb-0">CID: {{ $doctor->cid }}</p>
                    <p class="card-text mb-0">Name: {{ $doctor->name }}</p>
                    <p class="card-text mb-0">Email: {{ $doctor->email }}</p>
                    <p class="card-text mb-0">Specialization: {{ $doctor->specialization }}</p>
                   <p class="card-text mb-0">Leave Status:
                        @if(isset($doctorStatus[$doctor->cid]))
                            {{ $doctorStatus[$doctor->cid] }}
                        @else
                            Status not available
                        @endif
                    </p>
                 </div>
            </div>
             {{-- <div class="dropdown" style="cursor: pointer; margin-left: 10px">
                <i class="fas fa-ellipsis-h " id="ellipsisToggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                <div class="dropdown-menu" aria-labelledby="ellipsisToggle">
                    <a class="dropdown-item" href="{{ route('admEditDoctor', ['cid' => $doctor->cid]) }}" title="Edit doctor">Edit</a>
                    <a class="dropdown-item" title="Edit doctor">Edit</a>
                    <form method="POST" action="{{ route('admDeleteLeave', ['id' => $leaves->id]) }}" accept-charset="UTF-8" style="display:inline">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="dropdown-item" title="Delete doctor" onclick="return confirm('Are you sure you want to delete doctor?')">Delete</button>
                    </form>

                </div>
            </div> --}}
         </div>
        @endforeach
    @endif

      @if(session('error'))
          <div class="alert alert-danger">
              {{ session('error') }}
          </div>
      @endif

</section>
</main>

</body>
<script src="{{ asset('assets/js/main.js') }}"></script>
<script>
    function navigateToDetail(url) {
        window.location.href = url;
    }
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</html>
