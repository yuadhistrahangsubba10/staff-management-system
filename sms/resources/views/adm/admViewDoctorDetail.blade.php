<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Staff Movement System</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link rel="stylesheet" href="{{ asset('assets/vendor/aos/aos.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-icons/bootstrap-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/boxicons/css/boxicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/glightbox/css/glightbox.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/swiper/swiper-bundle.min.css') }}">

  <!-- Template Main CSS File -->
  <link rel="stylesheet" href="{{ asset('assets/css/Detail.css') }}">
</head>

<body>
  <!-- ======= Mobile nav toggle button ======= -->
  <i class="bi bi-list mobile-nav-toggle d-xl-none"></i>
  <!-- ======= Header ======= -->
  <header id="header">
    <div class="d-flex flex-column">

      <div class="profile">
        <img src="{{ asset('assets/img/logo.png') }}" alt="No Logo" class="img-fluid">
        <h1 class="text-light"><a href="index.html"></a></h1>

      </div>
      <nav id="navbar" class="nav-menu navbar">
        <ul>
            <li><a href="{{ url('/adm/dashboard') }}" class="nav-link scrollto"><i class="fas fa-home"></i> <span>DashBoard</span></a></li>
            <li><a href="{{ url('/adm/admViewDoctor') }}" class="nav-link scrollto  active"><i class="fas fa-user-md"></i> <span>Doctor</span></a></li>
            <li><a  href="{{ url('/adm/admAddDoctor') }}" class="nav-link scrollto"><i class="fas fa-hospital"></i> <span>Add Doctor</span></a></li>
            <li><a href="{{ url('/adm/leave') }}" class="nav-link scrollto"><i class="fas fa-user-circle"></i> <span>Leave</span></a></li>
            <li><a href="{{ url('/adm/admProfile') }}" class="nav-link scrollto"><i class="fas fa-user-circle"></i> <span>Profile</span></a></li>
            <li>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <a href="#"  class="nav-link scrollto" onclick="event.preventDefault();this.closest('form').submit();"><i class="fas fa-sign-out-alt"></i><span>Log Out</span></a>
                </form>
            </li>
      </ul>
      </nav>
    </div>
  </header>



<main id="main">
 <section id="hero" class="about">
  <div class="container text-left">

      <div class="d-flex justify-content-between align-items-center " style="margin-bottom: 40px;">
        <h4 style="font-weight:800; margin: 0;">Doctor Detail</h4>
      </div>

    @if($doctor)
    <div class="row">
      <div class="image col-lg-4">
            @if($doctor->image)
                <img src="{{ asset('/storage/images/' . $doctor->image) }}" alt="{{ $doctor->name }}">
            @else
                No Image
            @endif
      </div>
      <div class=" col-lg-8 ">
        <div class="Detail">
            <p style="font-size: 14px;">
            <b>Profile</b><br>
            {{ $doctor->name }}<br>
            {{ $doctor->cid }}<br>
            {{ $doctor->gender }}<br><br>

            <b>Specialization</b><br>
            {{ $doctor->specialization }} <br><br>

            <b>Address</b><br>
            {{ $doctor->hospitalName }}<br>
            {{ $doctor->location }} <br>
            {{ $doctor->email }}<br>
            +975 {{ $doctor->phone }}
          </p>
        </div>
    </div>
     <div class="row">
      <div class="col-lg-12 mt-lg-4">
        <div class=" description text-left py-4" style="font-size: 14px;">
            <p>{{ $doctor->description }}
        </div>
      </div>
    </div>

    @else
        <p>No Doctor found with the specified cid.</p>
    @endif
</div>
</section>
</main>
</body>
<script src="{{ asset('assets/js/main.js') }}"></script>
</html>
