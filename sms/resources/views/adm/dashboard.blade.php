<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Staff Movement System</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <link rel="stylesheet" href="{{ asset('assets/vendor/aos/aos.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-icons/bootstrap-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/boxicons/css/boxicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/glightbox/css/glightbox.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/swiper/swiper-bundle.min.css') }}">

  <!-- Template Main CSS File -->
  <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/Doctor.css') }}">
</head>
<body>

  <!-- ======= Mobile nav toggle button ======= -->
  <i class="bi bi-list mobile-nav-toggle d-xl-none"></i>

  <!-- ======= Header ======= -->
  <header id="header">
    <div class="d-flex flex-column">

      <div class="profile">
        <img src="{{ asset('assets/img/logo.png') }}" alt="No Logo" class="img-fluid">
        <h1 class="text-light"><a href="index.html"></a></h1>

      </div>

      <nav id="navbar" class="nav-menu navbar">
        <ul>
          <li><a href="{{ url('/adm/dashboard') }}" class="nav-link scrollto  active"><i class="fas fa-home"></i> <span>DashBoard</span></a></li>
          <li><a href="{{ url('/adm/admViewDoctor') }}" class="nav-link scrollto"><i class="fas fa-user-md"></i> <span>Doctor</span></a></li>
          <li><a  href="{{ url('/adm/admAddDoctor') }}" class="nav-link scrollto"><i class="fas fa-hospital"></i> <span>Add Doctor</span></a></li>
          <li><a href="{{ url('/adm/leave') }}" class="nav-link scrollto"><i class="fas fa-user-circle"></i> <span>Leave</span></a></li>
          <li><a href="{{ url('/adm/admProfile') }}" class="nav-link scrollto"><i class="fas fa-user-circle"></i> <span>Profile</span></a></li>
          <li>
            <form method="POST" action="{{ route('logout') }}">
                @csrf
                <a href="#"  class="nav-link scrollto" onclick="event.preventDefault();this.closest('form').submit();"><i class="fas fa-sign-out-alt"></i><span>Log Out</span></a>
            </form>
        </li>
      </ul>
      </nav>
    </div>
  </header>

  <main id="main">
 <section id="hero" class="about">
  <div class="container text-left">
    <h4 style="font-weight:800;">Administrative Assistant Dashboard</h4>

    <div class="row">
        <div class="column">
          <div class="box" style="background-image: url('{{ asset('assets/img/hospital.jpeg') }}');">
              <h5>Total Doctor</h5>
              <p><?php echo App\Models\Doctors::count(); ?></p>
          </div>
        </div>
        <div class="column">
          <div class="box" style="background-image:  url('{{ asset('assets/img/hospital.jpeg') }}');">
              <h5>Total Director</h5>
              <p><?php echo App\Models\User::where('role', 'director')->count(); ?></p>
          </div>
        </div>
        <div class="column">
          <div class="box" style="background-image:  url('{{ asset('assets/img/hospital.jpeg') }}');">
              <h5>Total Adm</h5>
              <p><?php echo App\Models\User::where('role', 'director')->count(); ?></p>
          </div>
        </div>
        <div class="column">
          <div class="box" style="background-image:  url('{{ asset('assets/img/hospital.jpeg') }}');">
              <h5>Total Hospital</h5>
              <p><?php echo App\Models\Hospitals::count(); ?></p>
          </div>
        </div>
    </div>

    <div class="col-12 d-lg-flex">
        <div class="col-8">
            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif

            @if($doctors->isNotEmpty())
                    @foreach($doctors as $doctor)
                    <div class="card col-12" style="padding: 10px">
                        <div style="cursor: pointer; width:100%; display:flex; flex-direction:row;" onclick="navigateToDetail('{{ route('admViewDoctor', ['cid' => $doctor->cid]) }}')">
                            <img src="{{ asset('/storage/images/' . $doctor->image) }}" alt="{{ $doctor->name }}" class="card-img-top">
                            <div class="card-body" style="display: flex;flex-direction: row; align-items: center;">
                                <p class="card-text">
                                    <b>{{ $doctor->name }}</b><br>
                                    {{ $doctor->specialization }}<br>
                                    {{ $doctor->phone }}<br>
                                    {{ $doctor->email }}<br>

                                </p>
                            </div>
                        </div>


                        <div class="dropdown" style="cursor: pointer; margin-left: 10px">
                            <i class="fas fa-ellipsis-h " id="ellipsisToggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                            <div class="dropdown-menu" aria-labelledby="ellipsisToggle">
                                <a class="dropdown-item" href="{{ route('admEditDoctor', ['cid' => $doctor->cid]) }}" title="Edit doctor">Edit</a>
                                <form method="POST" action="{{ route('admDeleteDoctor', ['cid' => $doctor->cid]) }}" accept-charset="UTF-8" style="display:inline">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="dropdown-item" title="Delete doctor" onclick="return confirm('Are you sure you want to delete doctor?')">Delete</button>
                                </form>

                            </div>
                        </div>
                    </div>
                    @endforeach
                @else
                    <p>No hospitals found.</p>
                @endif

            @if(session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif

            </div>
                <div class="col-4" style="background-color: green;">hello</div>
            </div>







  </div>
</section>
</main>

<script src="{{ asset('assets/js/main.js') }}"></script>
<script>
    function navigateToDetail(url) {
        window.location.href = url;
    }
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>


