<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Staff Movement System</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->

  <link rel="stylesheet" href="{{ asset('assets/vendor/aos/aos.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-icons/bootstrap-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/boxicons/css/boxicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/glightbox/css/glightbox.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/swiper/swiper-bundle.min.css') }}">

  <!-- Template Main CSS File -->
  <link rel="stylesheet" href="{{ asset('assets/css/Doctor.css') }}">
</head>

<body>
  <!-- ======= Mobile nav toggle button ======= -->
  <i class="bi bi-list mobile-nav-toggle d-xl-none"></i>
  <!-- ======= Header ======= -->
  <header id="header">
    <div class="d-flex flex-column">

      <div class="profile">
        <img src="{{ asset('assets/img/logo.png') }}" alt="No Logo" class="img-fluid">
        <h1 class="text-light"><a href="index.html"></a></h1>

      </div>
      <nav id="navbar" class="nav-menu navbar">
        <ul>
            <li><a href="{{ url('/director/dashboard') }}" class="nav-link scrollto  active"><i class="fas fa-home"></i> <span>DashBoard</span></a></li>
            <li><a href="{{ url('/director/directorViewDoctor') }}" class="nav-link scrollto"><i class="fas fa-user-md"></i> <span>Doctor</span></a></li>
            <li><a href="{{ url('/director/directorProfile') }}" class="nav-link scrollto"><i class="fas fa-user-circle"></i> <span>Profile</span></a></li>
            {{-- <li><a href="" class="nav-link scrollto"><i class="fas fa-sign-out-alt"></i> <span>Logout</span></a></li> --}}
            <form method="POST" action="{{ route('logout') }}">
              @csrf
              <a href="{{ route('logout') }}" onclick="event.preventDefault();this.closest('form').submit();">
                  Log Out
              </a>
          </form>
      </ul>
      </nav>
    </div>
  </header>



<main id="main">
 <section id="hero" class="about">
  <div class="container text-left">

      <div class="d-flex justify-content-between align-items-center " style="margin-bottom: 40px;">
        <h4 style="font-weight:800; margin: 0;">Doctor List</h4>
          <div class="d-none d-md-flex justify-content-start align-items-center">
              <button class="btn btn-primary" type="button" style="margin-right: 30px;">Download</button>
              <button class="btn sort-button" style="color: aliceblue;">
                <i class="fas fa-sort sort-icon"></i> Sort
              </button>
          </div>
      </div>

      <div class="d-md-none mt-3 pr-md-3" style="margin-bottom: 40px;" >
          <button class="btn btn-primary " type="button">Download</button>
          <button class="btn sort-button" style="color: aliceblue;margin-right: 40px;">
            <i class="fas fa-sort sort-icon"></i> Sort
          </button>
      </div>

      <div class="card col-12" style="cursor: pointer;" onclick="navigateToDetail()">
        <img src="{{ asset('assets/img/thimphu.jpeg') }}" class="card-img-top" alt="Profile Picture">
        <div class="card-body">
            <p class="card-text">
                Name: Gyem Tshering<br>
                Email: gyempassang9@gmail.com<br>
                Profession: Cardiologist<br>
                Phone: 77398819<br>
            </p>
        </div>
        <button  class="status-btn" type="button">Not availabe</button>

            <a href="#" style="color: black; background-color: transparent;margin-right: 10px;"><i class="fas fa-ellipsis-h"></i></a>
    </div>

    <div class="card col-12">
      <img src="{{ asset('assets/img/thimphu.jpeg') }}" class="card-img-top" alt="Profile Picture">
      <div class="card-body">
          <p class="card-text">
              Name: Gyem Tshering<br>
              Email: gyempassang9@gmail.com<br>
              Profession: Cardiologist<br>
              Phone: 77398819<br>
          </p>
      </div>
      <button  class="status-btn" type="button">Not availabe</button>
        <a href="#" style="color: black; background-color: transparent;margin-right: 10px;"><i class="fas fa-ellipsis-h"></i></a>



    </div>

</section>
</main>

</body>
<script src="{{ asset('assets/js/main.js') }}"></script>
<script>
  function navigateToDetail() {
      window.location.href = 'detail.html';
  }
</script>
</html>
