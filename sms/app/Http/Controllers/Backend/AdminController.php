<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Hospitals;
use App\Models\Doctors;
use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewPasswordMail;

class AdminController extends Controller
{
    public function adminDashboard(){
        return view('admin.dashboard');
    }
    //Display User
    public function doctor(){
        return view('admin.viewDoctor');
    }
    public function director(){
        return view('admin.viewDirector');
    }
    public function hospital(){
        return view('admin.viewHospital');
    }
    public function adm(){
        return view('admin.viewAdm');
    }

    //Profile
    public function profile(){
        return view('admin.Profile');
    }

    // ADM
    public function admPage(){
        return view('admin.addAdm');
    }

     public function callAdm(){
        $users = User::where('role', 'adm')->get();
        return view('admin.viewAdm', compact('users'));
    }

    public function addAdmPost(Request $request) {

        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string',
            'phone' => 'required|string',
            'cid' => 'required|string',
            'location' => 'required|string',
            'hospitalName' => 'required|string',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        // Check if a user with the given CID already exists
        if (User::where('cid', $request->cid)->exists()) {
            return redirect()->back()->with('error', 'A user with this CID already exists.');
        }

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('images', $imageName, 'public');
        } else {
            $imageName = null;
        }

        $password = Str::random(10); // Generate a 10-character random string

        // Hash the password
        $hashedPassword = Hash::make($password);

        // Create a new user instance
        $users = new User();

        $users->name = $request->name;
        $users->email = $request->email;
        $users->phone = $request->phone;
        $users->cid = $request->cid;
        $users->location = $request->location;
        $users->hospitalName = $request->hospitalName;
        $users->role = 'adm';
        $users->image = $imageName;
        $users->password = $hashedPassword;
        $users->save();

        Mail::to($users->email)->send(new NewPasswordMail($password));

        return redirect()->route('callAdm')->with('success', 'Adminstrative Assitant successfully added.');
    }

    public function deleteAdm($cid){
        $users = User::where('cid', $cid)->first();

        if ($users) {
            $users->delete();
            return redirect()->back()->with('success', 'Adminstrative Assitant deleted successfully.');
        } else {
            return redirect()->back()->with('error', 'Adminstrative Assitant not found.');
        }
    }

    public function editAdm($cid){
        $users = User::where('cid', $cid)->first();

        return view('admin.editAdm', ['user' => $users]);
    }

    public function updateAdm(Request $request, $cid){
        $user = User::find($cid);

        if (!$user) {
            return redirect()->back()->with('error', 'Director not found.');
        }

        // Validate form data
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string',
            'phone' => 'required|string',
            'cid' => 'required|string',
            'location' => 'required|string',
            'hospitalName' => 'required|string',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        // Handle image upload
        if ($request->hasFile('image')) {
            // Delete previous image if exists
            if ($user->image) {
                Storage::disk('public')->delete('images/' . $user->image);
            }

            // Store new image
            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('images', $imageName, 'public');

            // Update hospital image field
            $user->image = $imageName;
        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->cid= $request->cid;
        $user->location = $request->location;
        $user->hospitalName = $request->hospitalName;

        $user->save();

        return redirect()->route('callAdm')->with('success', 'Adminstrative Assitant updated successfully.');
    }

    // Director
    public function directorPage(){
        return view('admin.addDirector');
    }

    public function callDirectors(){
        $users = User::where('role', 'director')->get();
        return view('admin.viewDirector', compact('users'));
    }

    public function addDirectorPost(Request $request) {

        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string',
            'phone' => 'required|string',
            'cid' => 'required|string',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        // Check if a user with the given CID already exists
        if (User::where('cid', $request->cid)->exists()) {
            return redirect()->back()->with('error', 'A user with this CID already exists.');
        }

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('images', $imageName, 'public');
        } else {
            $imageName = null;
        }

        $password = Str::random(10); // Generate a 10-character random string

        // Hash the password
        $hashedPassword = Hash::make($password);

        // Create a new user instance
        $users = new User();

        $users->name = $request->name;
        $users->email = $request->email;
        $users->phone = $request->phone;
        $users->cid = $request->cid;
        $users->location = 'Thimphu';
        $users->hospitalName = 'JDWNRH';
        $users->role = 'director';
        $users->image = $imageName;
        $users->password = $hashedPassword;
        $users->save();

        Mail::to($users->email)->send(new NewPasswordMail($password));

        return redirect()->route('callDirectors')->with('success', 'Director successfully added.');
    }

    public function deleteDirector($cid){
        $users = User::where('cid', $cid)->first();

        if ($users) {
            $users->delete();
            return redirect()->back()->with('success', 'Director deleted successfully.');
        } else {
            return redirect()->back()->with('error', 'Director not found.');
        }
    }

    public function editDirector($cid){
        $users = User::where('cid', $cid)->first();

        return view('admin.editDirector', ['user' => $users]);
    }

    public function updateDirector(Request $request, $cid){
        $user = User::find($cid);

        if (!$user) {
            return redirect()->back()->with('error', 'Director not found.');
        }

        // Validate form data
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string',
            'phone' => 'required|string',
            'cid' => 'required|string',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        // Handle image upload
        if ($request->hasFile('image')) {
            // Delete previous image if exists
            if ($user->image) {
                Storage::disk('public')->delete('images/' . $user->image);
            }

            // Store new image
            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('images', $imageName, 'public');

            // Update hospital image field
            $user->image = $imageName;
        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->cid= $request->cid;

        $user->save();

        return redirect()->route('callDirectors')->with('success', 'Director updated successfully.');
    }


    // Doctor
    public function doctorPage(){
        return view('admin.addDoctor');
    }
     // Call all Doctor
     public function callDoctors(){
        $doctors = Doctors::all();
        return view('admin.viewDoctor', compact('doctors'));
    }

    // View Doctor
    public function adminViewDoctor($cid){
        $doctor = Doctors::where('cid', $cid)->first();

        return view('admin.viewDoctorDetail', ['doctor' => $doctor]);
    }

    public function addDoctorPost(Request $request) {
        $request->validate([
            'cid' => 'required|string',
            'email' => 'required|string',
            'name' => 'required|string',
            'phone' => 'required|string',
            'specialization' => 'required|string',
            'gender' => 'required|string',
            'location' => 'required|string',
            'hospitalName' => 'required|string',
            'description' => 'required|string',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('images', $imageName, 'public');
        } else {
            $imageName = null;
        }

        $doctors = new Doctors();

        $doctors->cid = $request->cid;
        $doctors->email = $request->email;
        $doctors->name = $request->name;
        $doctors->phone = $request->phone;
        $doctors->specialization = $request->specialization;
        $doctors->gender = $request->gender;
        $doctors->location = $request->location;
        $doctors->hospitalName = $request->hospitalName;
        $doctors->description = $request->description;
        $doctors->image = $imageName;

        $doctors->save();

        return redirect()->route('callDoctors')->with('success', 'Doctor successfully added.');
    }

    public function deleteDoctor($cid){
        $doctors = Doctors::where('cid', $cid)->first();

        if ($doctors) {
            $doctors->delete();
            return redirect()->back()->with('success', 'Doctor deleted successfully.');
        } else {
            return redirect()->back()->with('error', 'Doctor not found.');
        }
    }


    public function editDoctor($cid){
        $doctors = Doctors::where('cid', $cid)->first();

        return view('admin.editDoctor', ['doctor' => $doctors]);
    }

    public function updateDoctor(Request $request, $cid){
        $doctor = Doctors::find($cid);

        if (!$doctor) {
            return redirect()->back()->with('error', 'Doctor not found.');
        }

        // Validate form data
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string',
            'phone' => 'required|string',
            'cid' => 'required|string',
            'specialization' => 'required|string',
            'gender' => 'required|string',
            'location' => 'required|string',
            'hospitalName' => 'required|string',
            'description' => 'required|string',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        if ($request->hasFile('image')) {
            if ($doctor->image) {
                Storage::disk('public')->delete('images/' . $doctor->image);
            }

            // Store new image
            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('images', $imageName, 'public');

            // Update hospital image field
            $doctor->image = $imageName;
        }

        $doctor->name = $request->name;
        $doctor->email = $request->email;
        $doctor->phone = $request->phone;
        $doctor->cid= $request->cid;
        $doctor->specialization = $request->specialization;
        $doctor->gender = $request->gender;
        $doctor->location = $request->location;
        $doctor->hospitalName = $request->hospitalName;
        $doctor->description = $request->description;

        $doctor->save();

        return redirect()->route('callDoctors')->with('success', 'Doctor updated successfully.');
    }

    // Hospital
    public function hospitalPage(){
        return view('admin.addHospital');
    }
    // Call all hospitals
    public function callHospitals(){
        $hospitals = Hospitals::all();
        return view('admin.viewHospital', compact('hospitals'));
    }




}
