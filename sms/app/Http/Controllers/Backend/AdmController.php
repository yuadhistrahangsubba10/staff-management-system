<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Doctors;
use App\Models\Leaves;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;


class AdmController extends Controller
{
    public function admDashboard(){
        $user = Auth::user();

        $doctors = Doctors::where('location', $user->location)
        ->where('hospitalName', $user->hospitalName)
        ->get();

        return view('adm.dashboard', compact('doctors'));
    }

    //Profile
    public function admProfile(){
        return view('adm.admProfile');
    }

    //Display User
    public function admDoctor(){
        return view('adm.admViewDoctor');
    }

    //Add Doctor
    public function admAddDoctor(){
        return view('adm.admAddDoctor');
    }

    public function admDoctorPage(){
        return view('adm.admAddDoctor');
    }
      // Call all Doctor
      public function admCallDoctors(){
        $user = Auth::user();

        $doctors = Doctors::where('location', $user->location)
        ->where('hospitalName', $user->hospitalName)
        ->get();

        return view('adm.admViewDoctor', compact('doctors'));
    }

    // View Doctor
    // public function admViewLeave(){
    //     $user = Auth::user();

    //     $leaves = Leaves::where('location', $user->location)
    //                     ->where('hospitalName', $user->hospitalName)
    //                     ->get();

    //     $doctorStatus = [];

    //     foreach ($leaves as $leave) {
    //         // Check if leaves cid != doctor's cid
    //         if ($leave->cid != $user->cid) {
    //             // If leaves cid != doctor's cid, doctor status is "available"
    //             $doctorStatus[$leave->cid] = "available";
    //         } else {
    //             // If leaves cid = doctor's cid, check leave dates
    //             $currentDate = now()->toDateString();
    //             if ($currentDate >= $leave->startDate && $currentDate <= $leave->endDate) {
    //                 // If today's date is within the leave period, doctor status is "not available"
    //                 $doctorStatus[$leave->cid] = "not available";
    //             } else {
    //                 // If today's date is not within the leave period, doctor status is "available"
    //                 $doctorStatus[$leave->cid] = "available";
    //             }
    //         }
    //     }

    //     return view('adm.admViewDoctor', ['leaves' => $leaves, 'doctorStatus' => $doctorStatus]);
    // }


    public function admViewLeave() {
        $user = Auth::user();
        $leaves = Leaves::all();

        $doctors = Doctors::where('location', $user->location)
                          ->where('hospitalName', $user->hospitalName)
                          ->get();
        $doctorStatus = [];

        foreach ($doctors as $doctor) {
            $leave = Leaves::where('cid', $doctor->cid)->first();

            if ($leave) {
                $currentDate = date('Y-m-d');
                if ($currentDate >= $leave->start && $currentDate <= $leave->end) {
                    // Current date is within leave period
                    $status = 'Not available';
                } else {
                    // Current date is not within leave period
                    $status = 'Available';
                }
            } else {
                // Doctor's CID is not present in leaves table
                $status = 'Available';
            }

            $doctorStatus[$doctor->cid] = $status;
        }
        return view('adm.admViewDoctor', ['doctors' => $doctors, 'leaves' => $leaves,'doctorStatus' => $doctorStatus]);
    }

    public function admViewDoctor($cid){
        $doctor = Doctors::where('cid', $cid)->first();

        return view('adm.admViewDoctorDetail', ['doctor' => $doctor]);
    }


    public function admAddDoctorPost(Request $request) {
        $user = Auth::user();

        $request->validate([
            'cid' => 'required|string',
            'email' => 'required|string',
            'name' => 'required|string',
            'phone' => 'required|string',
            'specialization' => 'required|string',
            'gender' => 'required|string',
            'description' => 'required|string',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('images', $imageName, 'public');
        } else {
            $imageName = null;
        }

        $doctors = new Doctors();

        $doctors->cid = $request->cid;
        $doctors->email = $request->email;
        $doctors->name = $request->name;
        $doctors->phone = $request->phone;
        $doctors->specialization = $request->specialization;
        $doctors->gender = $request->gender;
        $doctors->hospitalName = $user->hospitalName;
        $doctors->location = $user->location;
        $doctors->description = $request->description;
        $doctors->image = $imageName;

        $doctors->save();

        return redirect()->route('adm.dashboard')->with('success', 'Doctor successfully added.');
    }

    public function admEditDoctor($cid){
        $doctors = Doctors::where('cid', $cid)->first();

        return view('adm.admEditDoctor', ['doctor' => $doctors]);
    }

    public function admUpdateDoctor(Request $request, $cid){
        $doctor = Doctors::find($cid);

        if (!$doctor) {
            return redirect()->back()->with('error', 'Doctor not found.');
        }

        // Validate form data
        $request->validate([
            'cid' => 'required|string',
            'name' => 'required|string',
            'email' => 'required|string',
            'phone' => 'required|string',
            'specialization' => 'required|string',
            'gender' => 'required|string',
            'location' => 'required|string',
            'hospitalName' => 'required|string',
            'description' => 'required|string',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        if ($request->hasFile('image')) {
            if ($doctor->image) {
                Storage::disk('public')->delete('images/' . $doctor->image);
            }

            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('images', $imageName, 'public');

            $doctor->image = $imageName;
        }

        $doctor->name = $request->name;
        $doctor->email = $request->email;
        $doctor->phone = $request->phone;
        $doctor->cid= $request->cid;
        $doctor->specialization = $request->specialization;
        $doctor->gender = $request->gender;
        $doctor->location = $request->location;
        $doctor->hospitalName = $request->hospitalName;
        $doctor->description = $request->description;

        $doctor->save();

        return redirect()->route('adm.dashboard')->with('success', 'Doctor updated successfully.');
    }

    public function admDeleteDoctor($cid){
        $doctors = Doctors::where('cid', $cid)->first();

        if ($doctors) {
            $doctors->delete();
            return redirect()->back()->with('success', 'Doctor deleted successfully.');
        } else {
            return redirect()->back()->with('error', 'Doctor not found.');
        }
    }

    // Add Leave
    public function leave(){
        $user = Auth::user();

        $doctors = Doctors::where('location', $user->location)
        ->where('hospitalName', $user->hospitalName)
        ->get();

        return view('adm.leave', compact('doctors'));
    }

    public function admLeavePage(){
        return view('adm.leave');
    }

    public function admLeavePost(Request $request) {
        $request->validate([
            'cid' => 'required',
            'type' => 'required|string',
            'start' => 'required|date',
            'end' => 'required|date',
            'remarks' => 'required|string',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        $user = Auth::user();

        $doctor = Doctors::where('cid', $request->cid)->where('hospitalName', $user->hospitalName)->first();

        if (!$doctor) {
            return redirect()->back()->with('error', 'Doctor with provided CID not found.');
        }

    // Check for leave collision
    $leaveCollisions = Leaves::where('cid', $doctor->cid)
    ->where(function($query) use ($request) {
        $query->whereBetween('start', [$request->start, $request->end])
                ->orWhereBetween('end', [$request->start, $request->end])
                ->orWhere(function($query) use ($request) {
                    $query->where('start', '<', $request->start)
                        ->where('end', '>', $request->end);
                });
    })
    ->exists();

    if ($leaveCollisions) {
    return redirect()->route('admViewLeave')->with('error', 'Leave dates collide with existing leaves for this doctor.');
    }

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('images', $imageName, 'public');
        } else {
            $imageName = null;
        }

        $leave = new Leaves();

        $leave->cid = $doctor->cid;
        $leave->name = $doctor->name;
        $leave->start = $request->start;
        $leave->end = $request->end;
        $leave->location = $doctor->location;
        $leave->hospitalName = $doctor->hospitalName;
        $leave->type = $request->type;
        $leave->remarks = $request->remarks;
        $leave->image = $imageName;
        // $leave->profile= $doctor->image;

        $leave->save();

        return redirect()->route('admViewLeave')->with('success', 'Leave successfully added.');
    }

    public function admDeleteLeave($id){
        $leaves = Leaves::where('id', $id)->first();

        if ($leaves) {
            $leaves->delete();
            return redirect()->back()->with('success', 'Leave deleted successfully.');
        } else {
            return redirect()->back()->with('error', 'Leave not found.');
        }
    }

}
