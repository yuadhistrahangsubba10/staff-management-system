<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Hospitals;
use Illuminate\Support\Facades\Storage;

class HospitalController extends Controller
{
    public function addHospitalPost(Request $request) {

        $request->validate([
            'name' => 'required|string',
            'location' => 'required|string',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('images', $imageName, 'public');
        } else {
            $imageName = null;
        }

        $hospitals = new Hospitals();

        $hospitals->name = $request->name;
        $hospitals->location = $request->location;
        $hospitals->image = $imageName;

        $hospitals->save();

        return redirect()->route('callHospitals')->with('success', 'Hospital successfully added.');
    }

    public function deleteHospital($id){
        $hospitals = Hospitals::where('id', $id)->first();

        if ($hospitals) {
            $hospitals->delete();
            return redirect()->back()->with('success', 'Hospital deleted successfully.');
        } else {
            return redirect()->back()->with('error', 'Hospital not found.');
        }
    }

    public function editHospital($id){
        $hospitals = Hospitals::where('id', $id)->first();

        return view('admin.edithospital', ['hospital' => $hospitals]);
    }

    public function updateHospital(Request $request, $id){
        $hospital = Hospitals::find($id);

        if (!$hospital) {
            return redirect()->back()->with('error', 'Hospital not found.');
        }

        // Validate form data
        $request->validate([
            'name' => 'required|string',
            'location' => 'required|string',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        // Handle image upload
        if ($request->hasFile('image')) {
            // Delete previous image if exists
            if ($hospital->image) {
                Storage::disk('public')->delete('images/' . $hospital->image);
            }

            // Store new image
            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('images', $imageName, 'public');

            // Update hospital image field
            $hospital->image = $imageName;
        }

        $hospital->name = $request->name;
        $hospital->location = $request->location;
        $hospital->save();

        return redirect()->route('callHospitals')->with('success', 'Hospital updated successfully.');
    }
}
