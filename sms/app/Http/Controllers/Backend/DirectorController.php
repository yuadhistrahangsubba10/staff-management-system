<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DirectorController extends Controller
{
    public function directorDashboard(){
        return view('director.dashboard');
    }

     //Profile
     public function directorProfile(){
        return view('director.directorProfile');
    }

    //Display User
    public function directorDoctor(){
        return view('director.directorViewDoctor');
    }
}
