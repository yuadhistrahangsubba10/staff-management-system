<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Hospitals;
use Illuminate\Support\Facades\DB;

class DownloadController extends Controller
{
    public function downloadHospital(Request $request){
        $hospitals = DB::table('hospitals')->select('id', 'name', 'location',)->get();

        if ($hospitals->isEmpty()) {
            return redirect()->back()->with('error', 'No hospitals were found in the database.');
        }

        $csvFileName = 'Hospital.csv';

        $handle = fopen('php://memory', 'r+');

        $header = array_keys((array) $hospitals[0]);
        fputcsv($handle, $header);

        foreach ($hospitals as $row) {
            unset($row->created_at, $row->updated_at);
            fputcsv($handle, (array) $row);
        }

        rewind($handle);

        $content = stream_get_contents($handle);
        fclose($handle);

        $csvFileName = 'Hospital.csv';

        $headers = [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="' . $csvFileName . '"',
        ];

        // Return CSV file if needed
        return response($content, 200, $headers);
    }

    public function downloadDoctor(Request $request){
        $doctors = DB::table('doctors')->select( 'cid', 'email', 'name', 'phone', 'specialization', 'gender', 'location','hospitalName','description',)->get();

        if ($doctors->isEmpty()) {
            return redirect()->back()->with('error', 'No Docotor were found in the database.');
        }

        $csvFileName = 'Doctor.csv';

        $handle = fopen('php://memory', 'r+');

        $header = array_keys((array) $doctors[0]);
        fputcsv($handle, $header);

        foreach ($doctors as $row) {
            unset($row->created_at, $row->updated_at);
            fputcsv($handle, (array) $row);
        }

        rewind($handle);

        $content = stream_get_contents($handle);
        fclose($handle);

        $csvFileName = 'Doctor.csv';

        $headers = [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="' . $csvFileName . '"',
        ];

        // Return CSV file if needed
        return response($content, 200, $headers);
    }


    // Download Director
    public function downloadDirector(Request $request){
        $users = DB::table('users')->select('cid', 'name', 'email','phone') ->where('role', 'director')->get();

        if ($users->isEmpty()) {
            return redirect()->back()->with('error', 'No Users were found in the database.');
        }

        $csvFileName = 'Director.csv';

        $handle = fopen('php://memory', 'r+');

        $header = array_keys((array) $users[0]);
        fputcsv($handle, $header);

        foreach ($users as $row) {
            unset($row->created_at, $row->updated_at);
            fputcsv($handle, (array) $row);
        }

        rewind($handle);

        $content = stream_get_contents($handle);
        fclose($handle);

        $csvFileName = 'Director.csv';

        $headers = [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="' . $csvFileName . '"',
        ];

        // Return CSV file if needed
        return response($content, 200, $headers);
    }

    // Download Director
    public function downloadAdm(Request $request){
        $users = DB::table('users')->select('cid', 'name', 'email','phone') ->where('role', 'adm')->get();

        if ($users->isEmpty()) {
            return redirect()->back()->with('error', 'No Users were found in the database.');
        }

        $csvFileName = 'ADM.csv';

        $handle = fopen('php://memory', 'r+');

        $header = array_keys((array) $users[0]);
        fputcsv($handle, $header);

        foreach ($users as $row) {
            unset($row->created_at, $row->updated_at);
            fputcsv($handle, (array) $row);
        }

        rewind($handle);

        $content = stream_get_contents($handle);
        fclose($handle);

        $csvFileName = 'ADM.csv';

        $headers = [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="' . $csvFileName . '"',
        ];

        // Return CSV file if needed
        return response($content, 200, $headers);
    }

    // Download Director
    public function downloadLeave(Request $request){
        $leaves = DB::table('leaves')->select('cid', 'name', 'start','end', 'type', 'location', 'hospitalName', 'remarks')->get();

        if ($leaves->isEmpty()) {
            return redirect()->back()->with('error', 'No leaves were found in the database.');
        }

        $csvFileName = 'Leave.csv';

        $handle = fopen('php://memory', 'r+');

        $header = array_keys((array) $leaves[0]);
        fputcsv($handle, $header);

        foreach ($leaves as $row) {
            unset($row->created_at, $row->updated_at);
            fputcsv($handle, (array) $row);
        }

        rewind($handle);

        $content = stream_get_contents($handle);
        fclose($handle);

        $csvFileName = 'Leave.csv';

        $headers = [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="' . $csvFileName . '"',
        ];

        // Return CSV file if needed
        return response($content, 200, $headers);
    }
}
